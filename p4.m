%% Computational Mathematics - Problem Sheet 4
% Tobias Bretschneider

%% Part 1
%

% A function which generates the first $n$ Fibonacci numbers in a vector.

type fibonacci

% Now testing our function for $n=10$

fibonacci(10)

%% Part 2 
%

% A function that outputs the $n$th approximation to the golden ration.

type nthapprox

% Testing 

nthapprox(3)

nthapprox(5)

%% Part 3
%
counter=0;
qual = 1;
approx=[];
errors = [];
while qual > 10^(-7)
	counter=counter+1;
	approx(counter) = nthapprox(counter);
	qual = abs(approx(counter) - (1+sqrt(5))/2);
	errors(counter)=qual;
end

approx(int64(counter))
fib=fibonacci(counter+1);

% Here we output it as a fraction.

frac = fib(counter+1) + "/" + fib(counter)

% Checking our error
errors(counter)

% Printing what value of n was needed.

counter
%% Part 4
%

% Here we plot our graph.

n = 1:counter;
figure(1)
plot(n,errors)
title('Error against n')
xlabel('n')
ylabel('error')
figure(2)
plot(n,log(errors))
title('log(Error) against n')
xlabel('n')
ylabel('error')
