function nth = nthapprox(n)
	fib = fibonacci(int64(n+1));
	nth = fib(int64(n+1))/fib(n);
end
	
